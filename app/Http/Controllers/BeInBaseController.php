<?php


namespace App\Http\Controllers;


use App\Http\Response;

class BeInBaseController extends Controller
{
    public function ok($data, $message = "Data returned successfully")
    {
        return response()->json(new Response($data, $message), 200);
    }

    public function badRequest($message)
    {
        return response()->json(new Response(null, $message), 400);
    }

    public function forbidden($message)
    {
        return response()->json(new Response(null, $message), 403);
    }

    public function deleted($message = "Deleted Successfully")
    {
        return $this->ok(null, $message);
    }

    public function notFound($message = "The requested resource is not found")
    {
        return response()->json(new Response(null, $message), 404);
    }

    public function internalError($message = "Sorry, Something wrong happened at out side, please try again in a few moments")
    {
        return response()->json(new Response(null, $message), 500);
    }
}

<?php

namespace App\Http\Controllers;

use App\Transformers\UserResource;
use App\User;

class UserController extends BeInBaseController
{
    public function index()
    {
        $users = User::orderBy('id', 'desc')->get();
        return $this->ok(UserResource::collection($users));
    }
}

<?php

namespace App\Http\Controllers;

use App\Entities\Appointment;
use App\Entities\AppointmentDefinition;
use App\Entities\Expert;
use App\Transformers\AppointmentResource;
use App\Transformers\ExpertCalendarResource;
use App\Transformers\ExpertDetailsResource;
use App\Transformers\ExpertHeaderResource;
use App\Transformers\PaginationResource;
use App\Utils\ConversionUtil;
use Illuminate\Http\Request;

class ExpertController extends BeInBaseController
{
    public function index()
    {
        $experts = Expert::with(['specialization'])->paginate();
        return new PaginationResource(ExpertHeaderResource::class, $experts);
    }

    public function show($id)
    {
        $expert = Expert::with(['specialization', 'country', 'workingHours'])->findOrFail($id);
        return $this->ok(new ExpertDetailsResource($expert));
    }

    public function calendar(Request $request, $expertId)
    {
        $tz = ConversionUtil::getTimezone();
//        $tz = 'America/Phoenix';
        $request->merge([ 'tz' => $tz ]);
        $expert = Expert::with('workingHours')->findOrFail($expertId);
        $from = ConversionUtil::localToUtc($request->get('day') . ' 00:00:00', $tz);
        $to = ConversionUtil::localToUtc($request->get('day') . ' 23:59:59', $tz);

        $appointments = $expert->appointmentsBetween($from, $to)->get();
        $expert->appointments = $appointments->sortBy('from');
        return $this->ok(new ExpertCalendarResource($expert));
    }

    public function bookAppointment(Request $request, $expertId)
    {
        $tz = ConversionUtil::getTimezone();
//        $tz = 'America/Phoenix';
        $request->merge([ 'tz' => $tz ]);
        $appointment = Appointment::create([
            AppointmentDefinition::EXPERT_ID => $expertId,
            AppointmentDefinition::USER_ID => $request->get('userId'),
            AppointmentDefinition::DURATION => $request->get('duration'),
            AppointmentDefinition::FROM => ConversionUtil::localToUtc($request->get('from'), $tz),
            AppointmentDefinition::TO => ConversionUtil::localToUtc($request->get('to'), $tz),
        ]);
        return $this->ok(new AppointmentResource($appointment));
    }
}

<?php


namespace App\Http;


class Response
{
    public $data;
    public $message;

    public function __construct($data = null, $message = null)
    {
        $this->data = $data;
        $this->message = $message;
    }
}

<?php


namespace App\Utils;

use Carbon\Carbon;

class ConversionUtil
{
    public static function localToUtc($dateTime, $sourceTz)
    {
        // Mark with local timezone
        $dateTime = Carbon::createFromFormat('Y-m-d H:i:s', $dateTime, $sourceTz);
        $dateTime->setTimezone('UTC');
        return $dateTime;
    }

    public static function utcToLocal($dateTime, $targetTz)
    {
        // Mark as utc
        $dateTime = Carbon::createFromFormat('Y-m-d H:i:s', $dateTime, 'UTC');
        // Convert to local
        $dateTime->setTimezone($targetTz);
        return $dateTime;
    }

    public static function getTimezoneOffset($timezone)
    {
        return Carbon::now($timezone)->offsetHours;
    }

    private static function getClientIp(): string
    {
        $ip = request()->ip();
        return $ip == '127.0.0.1' ? '104.206.205.91' : $ip;
    }

    public static function getTimezone()
    {
        if ($timezone = request()->get('tz')) {
            return $timezone;
        }

        $ip = self::getClientIp();
        try {
            $response = json_decode(file_get_contents("http://freegeoip.app/json/{$ip}"), true);
            return $response['time_zone'];
        } catch (\Exception $e) {
            report($e);
            return '';
        }
    }
}

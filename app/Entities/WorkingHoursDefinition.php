<?php


namespace App\Entities;


class WorkingHoursDefinition
{
    const TABLE_NAME = 'working_hours';

    const ID = BaseFields::ID;
    const EXPERT_ID = 'expert_id';
    const OPEN_TIME = 'open_time';
    const CLOSE_TIME = 'close_time';

    const FILLABLES = [
        self::EXPERT_ID,
        self::OPEN_TIME,
        self::CLOSE_TIME
    ];
}

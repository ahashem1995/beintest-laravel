<?php

namespace App\Entities;

class Specialization extends BaseModel
{
    protected $table = SpecializationDefinition::TABLE_NAME;

    protected $fillable = SpecializationDefinition::FILLABLES;

    public function experts()
    {
        return $this->hasMany("App\Entities\Expert");
    }
}

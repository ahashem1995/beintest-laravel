<?php

namespace App\Entities;

class Country extends BaseModel
{
    protected $table = CountryDefinition::TABLE_NAME;

    protected $fillable = CountryDefinition::FILLABLES;

    public function experts()
    {
        return $this->hasMany("App\Entities\Expert");
    }
}

<?php


namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    protected $hidden = [
        BaseFields::CREATED_AT,
        BaseFields::UPDATED_AT
    ];

    public static function getClass() {
        return get_called_class();
    }

    public static function getTableName() {
        return (new static)->getTable();
    }
}

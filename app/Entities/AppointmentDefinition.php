<?php


namespace App\Entities;


class AppointmentDefinition
{
    const TABLE_NAME = 'appointments';

    const ID = BaseFields::ID;
    const EXPERT_ID = 'expert_id';
    const USER_ID = 'user_id';
    const DURATION = 'duration';
    const FROM = 'from';
    const TO = 'to';

    const FILLABLES = [
        self::EXPERT_ID,
        self::USER_ID,
        self::DURATION,
        self::FROM,
        self::TO,
    ];
}

<?php


namespace App\Entities;


class SpecializationDefinition
{
    const TABLE_NAME = 'specializations';

    const ID = BaseFields::ID;
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const PARENT_ID = 'parent_id';

    const FILLABLES = [
        self::NAME,
        self::DESCRIPTION,
        self::PARENT_ID,
    ];
}

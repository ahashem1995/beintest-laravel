<?php

namespace App\Entities;

class Appointment extends BaseModel
{
    protected $table = AppointmentDefinition::TABLE_NAME;

    protected $fillable = AppointmentDefinition::FILLABLES;

    public function expert()
    {
        return $this->belongsTo("App\Entities\Expert");
    }

    public function user()
    {
        return $this->belongsTo("App\User");
    }
}

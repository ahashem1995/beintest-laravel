<?php

namespace App\Entities;

class Expert extends BaseModel
{
    protected $table = ExpertDefinition::TABLE_NAME;

    protected $fillable = ExpertDefinition::FILLABLES;

    public function specialization()
    {
        return $this->belongsTo("App\Entities\Specialization");
    }

    public function country()
    {
        return $this->belongsTo("App\Entities\Country");
    }

    public function workingHours()
    {
        return $this->hasOne("App\Entities\WorkingHours");
    }

    public function appointments()
    {
        return $this->hasMany("App\Entities\Appointment");
    }

    public function appointmentsBetween($from, $to)
    {
        return $this->hasMany("App\Entities\Appointment")
            ->where([['from', '>=', $from], ['to', '<=', $to]]);
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
}

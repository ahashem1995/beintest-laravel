<?php

namespace App\Entities;

class WorkingHours extends BaseModel
{
    protected $table = WorkingHoursDefinition::TABLE_NAME;

    protected $fillable = WorkingHoursDefinition::FILLABLES;

    public function expert()
    {
        return $this->belongsTo("App\Entities\Expert");
    }
}

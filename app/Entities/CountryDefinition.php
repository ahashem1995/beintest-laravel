<?php


namespace App\Entities;


class CountryDefinition
{
    const TABLE_NAME = 'countries';

    const ID = BaseFields::ID;
    const NAME = 'name';

    const FILLABLES = [
        self::NAME,
    ];
}

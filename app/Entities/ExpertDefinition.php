<?php


namespace App\Entities;


class ExpertDefinition
{
    const TABLE_NAME = 'experts';

    const ID = BaseFields::ID;
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const EMAIL = 'email';
    const MOBILE_NUMBER = 'mobile_number';
    const PROFILE_PICTURE = 'profile_picture';
    const SPECIALIZATION_ID = 'specialization_id';
    const COUNTRY_ID = 'country_id';
    const TIMEZONE = 'timezone';

    const FILLABLES = [
        self::FIRST_NAME,
        self::LAST_NAME,
        self::EMAIL,
        self::MOBILE_NUMBER,
        self::PROFILE_PICTURE,
        self::SPECIALIZATION_ID,
        self::TIMEZONE
    ];
}

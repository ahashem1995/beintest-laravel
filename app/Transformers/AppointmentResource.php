<?php

namespace App\Transformers;

use App\Entities\AppointmentDefinition;
use App\Utils\ConversionUtil;
use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $from = $this[AppointmentDefinition::FROM];
        $to = $this[AppointmentDefinition::TO];
        $tz = $request->get('tz');
        return [
            'userId' => $this[AppointmentDefinition::USER_ID],
            'from' => ConversionUtil::utcToLocal($from, $tz)->format('Y-m-d H:i:s'),
            'to' => ConversionUtil::utcToLocal($to, $tz)->format('Y-m-d H:i:s'),
            'duration' => $this[AppointmentDefinition::DURATION]
        ];
    }
}

<?php

namespace App\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ExpertCalendarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'appointments' => AppointmentResource::collection($this->appointments),
            'workingHours' => new WorkingHoursResource($this->workingHours, $request->get('tz'), 'H:i:s')
        ];
    }
}

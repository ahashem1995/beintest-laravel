<?php


namespace App\Transformers;


use Illuminate\Http\Resources\Json\ResourceCollection;

class PaginationResource extends ResourceCollection
{
    private $className;

    public function __construct($resourceClass, $resource)
    {
        parent::__construct($resource);

        $this->className = $resourceClass;
    }

    public function toArray($request)
    {
        $this->collection->transform(function ($item) {
            return (new $this->className($item));
        });
        return parent::toArray($request);
    }
}

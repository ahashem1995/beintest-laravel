<?php

namespace App\Transformers;

use App\Entities\ExpertDefinition;
use App\Entities\SpecializationDefinition;
use Illuminate\Http\Resources\Json\JsonResource;

class ExpertHeaderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this[ExpertDefinition::ID],
            'fullName' => $this->fullName,
            'profilePicture' => $this[ExpertDefinition::PROFILE_PICTURE],
            'specialization' => $this->specialization[SpecializationDefinition::NAME],
        ];
    }
}

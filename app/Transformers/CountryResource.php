<?php

namespace App\Transformers;

use App\Entities\CountryDefinition;
use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this[CountryDefinition::ID],
            'name' => $this[CountryDefinition::NAME]
        ];
    }
}

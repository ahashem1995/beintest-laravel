<?php

namespace App\Transformers;

use App\Entities\WorkingHoursDefinition;
use App\Utils\ConversionUtil;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class WorkingHoursResource extends JsonResource
{
    private $timezone;
    private $format;

    public function __construct($resource, $timezone = null, $format = 'h:i A')
    {
        parent::__construct($resource);
        $this->timezone = $timezone;
        $this->format = $format;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $tzHoursOffset = ConversionUtil::getTimezoneOffset($this->timezone);
        return [
            'openTime' => Carbon::createFromFormat("H:i:s", $this[WorkingHoursDefinition::OPEN_TIME])
                ->addHours($tzHoursOffset)->format($this->format),
            'closeTime' => Carbon::createFromFormat("H:i:s", $this[WorkingHoursDefinition::CLOSE_TIME])
                ->addHours($tzHoursOffset)->format($this->format)
        ];
    }
}

<?php

namespace App\Transformers;

use App\Entities\CountryDefinition;
use App\Entities\ExpertDefinition;
use Illuminate\Http\Request;

class ExpertDetailsResource extends ExpertHeaderResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $returnValue = parent::toArray($request);
        $returnValue['workingHours'] = $this->when($this->workingHours,
            new WorkingHoursResource($this->workingHours, $this[ExpertDefinition::TIMEZONE]));
        $returnValue['country'] = $this->country[CountryDefinition::NAME];
        return $returnValue;
    }
}

<?php

namespace App\Transformers;

use App\UserDefinition;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this[UserDefinition::ID],
            'name' => $this[UserDefinition::NAME],
            'email' => $this[UserDefinition::EMAIL],
            'timezone' => $this[UserDefinition::TIMEZONE],
        ];
    }
}

<?php


namespace App;


use App\Entities\BaseFields;

class UserDefinition
{
    const TABLE_NAME = 'users';

    const ID = BaseFields::ID;
    const NAME = 'name';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const TIMEZONE = 'timezone';
    const EMAIL_VERIFIED_AT = 'email_verified_at';

    const FILLABLES = [
        self::NAME,
        self::EMAIL,
        self::PASSWORD,
        self::TIMEZONE,
    ];
}

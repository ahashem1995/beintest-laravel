<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api'
], function () {
    Route::prefix('v1')->group(function () {
        Route::get('/users', 'UserController@index')
            ->name('users.index');
        Route::prefix('experts')->group(function () {
            Route::get("/{id}", 'ExpertController@show')
                ->name('experts.show');
            Route::get("", 'ExpertController@index')
                ->name('experts.index');
            Route::get("/{id}/calendar", 'ExpertController@calendar')
                ->name('experts.calendar');
            Route::post("/{id}/appointment", 'ExpertController@bookAppointment')
                ->name('experts.appointment.store');
        });
    });
});

<?php

use App\Entities\Specialization;
use App\Entities\SpecializationDefinition;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SpecializationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        try {
            $instructor = Specialization::create([
                SpecializationDefinition::NAME => 'Instructor',
                SpecializationDefinition::DESCRIPTION => 'A person who trains or teaches others.',
                SpecializationDefinition::PARENT_ID => null,
            ]);

            Specialization::create([
                SpecializationDefinition::NAME => 'Chinese Teacher',
                SpecializationDefinition::DESCRIPTION => 'Chinese language teacher.',
                SpecializationDefinition::PARENT_ID => $instructor[SpecializationDefinition::ID],
            ]);

            $engineer = Specialization::create([
                SpecializationDefinition::NAME => 'Engineer',
                SpecializationDefinition::DESCRIPTION => 'A person who designs, builds, or maintains engines, machines, or public works.',
                SpecializationDefinition::PARENT_ID => null,
            ]);

            Specialization::create([
                SpecializationDefinition::NAME => 'Civil Engineer',
                SpecializationDefinition::DESCRIPTION => 'An engineer who designs and maintains roads, bridges, dams, and similar structures.',
                SpecializationDefinition::PARENT_ID => $engineer[SpecializationDefinition::ID],
            ]);

            Specialization::create([
                SpecializationDefinition::NAME => 'Computer Engineer',
                SpecializationDefinition::DESCRIPTION => 'An engineer who integrates several fields of computer science and electronic engineering required to develop computer hardware and software.',
                SpecializationDefinition::PARENT_ID => $engineer[SpecializationDefinition::ID],
            ]);
        } catch (Exception $exception) {
            $this->command->error($exception->getMessage());
            report($exception);
        }
    }
}

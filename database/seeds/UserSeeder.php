<?php

use App\Entities\BaseFields;
use App\User;
use App\UserDefinition;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        try {
            $faker = Factory::create();
            $fakedUsers = [];
            for ($i = 0; $i < 9; $i++) {
                $fakedUsers[] = [
                    UserDefinition::NAME => $faker->name,
                    UserDefinition::EMAIL => $faker->email,
                    UserDefinition::TIMEZONE => $faker->timezone,
                    UserDefinition::PASSWORD => Hash::make('123456'),
                    UserDefinition::EMAIL_VERIFIED_AT => Carbon::now(),
                    BaseFields::CREATED_AT => Carbon::now(),
                    BaseFields::UPDATED_AT => Carbon::now(),
                ];
            }
            $fakedUsers[] = [
                UserDefinition::NAME => 'Abdulrahman Hashem',
                UserDefinition::EMAIL => 'abd.alrahmanh.1995@gmail.com',
                UserDefinition::TIMEZONE => 'Asia/Damascus',
                UserDefinition::PASSWORD => Hash::make('123456'),
                UserDefinition::EMAIL_VERIFIED_AT => Carbon::now(),
                BaseFields::CREATED_AT => Carbon::now(),
                BaseFields::UPDATED_AT => Carbon::now(),
            ];
            User::insert($fakedUsers);
        } catch (Exception $exception) {
            $this->command->error($exception->getMessage());
            report($exception);
        }
    }
}

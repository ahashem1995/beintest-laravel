<?php

use App\Entities\Appointment;
use App\Entities\AppointmentDefinition;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class AppointmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        try {
            $jsonFile = storage_path() . '/database/appointments.json';
            $data = json_decode(file_get_contents($jsonFile));
            foreach ($data as $appointment) {
                Appointment::create([
                    AppointmentDefinition::ID => $appointment->id,
                    AppointmentDefinition::EXPERT_ID => $appointment->expert_id,
                    AppointmentDefinition::USER_ID => $appointment->user_id,
                    AppointmentDefinition::DURATION => $appointment->duration,
                    AppointmentDefinition::FROM => $appointment->from,
                    AppointmentDefinition::TO => $appointment->to,
                ]);
            }
        } catch (Exception $exception) {
            $this->command->error($exception->getTraceAsString());
            report($exception);
        }
    }
}

<?php

use App\Entities\Country;
use App\Entities\CountryDefinition;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        try {
            Country::create([
                CountryDefinition::NAME => 'China',
            ]);
            Country::create([
                CountryDefinition::NAME => 'Syria',
            ]);
            Country::create([
                CountryDefinition::NAME => 'Egypt',
            ]);
        } catch (Exception $exception) {
            $this->command->error($exception->getMessage());
            report($exception);
        }
    }
}

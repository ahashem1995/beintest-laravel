<?php

use App\Entities\Appointment;
use App\Entities\Country;
use App\Entities\Expert;
use App\Entities\Specialization;
use App\Entities\WorkingHours;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        try {
            Schema::disableForeignKeyConstraints();
            User::truncate();
            Country::truncate();
            Specialization::truncate();
            Appointment::truncate();
            WorkingHours::truncate();
            Expert::truncate();
            Schema::enableForeignKeyConstraints();

            $this->call(UserSeeder::class);
            $this->call(CountrySeeder::class);
            $this->call(SpecializationSeeder::class);
            $this->call(ExpertSeeder::class);
            $this->call(WorkingHoursSeeder::class);
            $this->call(AppointmentSeeder::class);

        } catch (Exception $exception) {
            $this->command->error($exception->getMessage());
        }
    }
}

<?php

use App\Entities\WorkingHours;
use App\Entities\WorkingHoursDefinition;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class WorkingHoursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        try {
            WorkingHours::create([
                WorkingHoursDefinition::EXPERT_ID => 1,
                WorkingHoursDefinition::OPEN_TIME => "01:00:00",
                WorkingHoursDefinition::CLOSE_TIME => "09:00:00",
            ]);
            WorkingHours::create([
                WorkingHoursDefinition::EXPERT_ID => 2,
                WorkingHoursDefinition::OPEN_TIME => "03:00:00",
                WorkingHoursDefinition::CLOSE_TIME => "09:00:00",
            ]);
            WorkingHours::create([
                WorkingHoursDefinition::EXPERT_ID => 3,
                WorkingHoursDefinition::OPEN_TIME => "11:00:00",
                WorkingHoursDefinition::CLOSE_TIME => "12:00:00",
            ]);
        } catch (Exception $exception) {
            $this->command->error($exception->getMessage());
            report($exception);
        }
    }
}

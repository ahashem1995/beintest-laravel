<?php

use App\Entities\Expert;
use App\Entities\ExpertDefinition;
use Faker\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\URL;

class ExpertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        try {
            $faker = Factory::create();
            Expert::create([
                ExpertDefinition::FIRST_NAME => 'Li',
                ExpertDefinition::LAST_NAME => 'Wei',
                ExpertDefinition::EMAIL => 'liwei@gmail.com',
                ExpertDefinition::MOBILE_NUMBER => $faker->e164PhoneNumber,
                ExpertDefinition::PROFILE_PICTURE => str_replace('http://localhost', 'http://localhost:8000', URL::asset("images/profile-img.jpg")),
                ExpertDefinition::SPECIALIZATION_ID => 2,
                ExpertDefinition::COUNTRY_ID => 1,
                ExpertDefinition::TIMEZONE => 'Asia/Shanghai',
            ]);
            Expert::create([
                ExpertDefinition::FIRST_NAME => 'Quasi',
                ExpertDefinition::LAST_NAME => 'Shawa',
                ExpertDefinition::EMAIL => 'qusaishawa@gmail.com',
                ExpertDefinition::MOBILE_NUMBER => $faker->e164PhoneNumber,
                ExpertDefinition::PROFILE_PICTURE => str_replace('http://localhost', 'http://localhost:8000', URL::asset("images/profile-img.jpg")),
                ExpertDefinition::SPECIALIZATION_ID => 4,
                ExpertDefinition::COUNTRY_ID => 2,
                ExpertDefinition::TIMEZONE => 'Asia/Damascus',
            ]);
            Expert::create([
                ExpertDefinition::FIRST_NAME => 'Shimaa',
                ExpertDefinition::LAST_NAME => 'Badawy',
                ExpertDefinition::EMAIL => 'shimaabadawi@gmail.com',
                ExpertDefinition::MOBILE_NUMBER => $faker->e164PhoneNumber,
                ExpertDefinition::PROFILE_PICTURE => str_replace('http://localhost', 'http://localhost:8000', URL::asset("images/profile-img.jpg")),
                ExpertDefinition::SPECIALIZATION_ID => 5,
                ExpertDefinition::COUNTRY_ID => 3,
                ExpertDefinition::TIMEZONE => 'Africa/Cairo',
            ]);
        } catch (Exception $exception) {
            $this->command->error($exception->getMessage());
            report($exception);
        }
    }
}

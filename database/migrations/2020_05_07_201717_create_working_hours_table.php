<?php

use App\Entities\ExpertDefinition;
use App\Entities\WorkingHoursDefinition;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkingHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(WorkingHoursDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->increments(WorkingHoursDefinition::ID);
            $table->unsignedInteger(WorkingHoursDefinition::EXPERT_ID);
            $table->time(WorkingHoursDefinition::OPEN_TIME);
            $table->time(WorkingHoursDefinition::CLOSE_TIME);

            $table->timestamps();
        });

        Schema::table(WorkingHoursDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->foreign(WorkingHoursDefinition::EXPERT_ID)->references(ExpertDefinition::ID)
                ->on(ExpertDefinition::TABLE_NAME);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(WorkingHoursDefinition::TABLE_NAME);
    }
}

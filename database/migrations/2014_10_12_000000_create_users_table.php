<?php

use App\UserDefinition;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(UserDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(UserDefinition::NAME);
            $table->string(UserDefinition::EMAIL)->unique();
            $table->string(UserDefinition::TIMEZONE)->nullable();
            $table->timestamp(UserDefinition::EMAIL_VERIFIED_AT)->nullable();
            $table->string(UserDefinition::PASSWORD);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(UserDefinition::TABLE_NAME);
    }
}

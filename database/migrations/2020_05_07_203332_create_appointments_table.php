<?php

use App\Entities\AppointmentDefinition;
use App\Entities\ExpertDefinition;
use App\UserDefinition;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AppointmentDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->increments(AppointmentDefinition::ID);
            $table->unsignedInteger(AppointmentDefinition::EXPERT_ID);
            $table->unsignedBigInteger(AppointmentDefinition::USER_ID);
            $table->integer(AppointmentDefinition::DURATION);
            $table->dateTime(AppointmentDefinition::FROM);
            $table->dateTime(AppointmentDefinition::TO);

            $table->timestamps();
        });

        Schema::table(AppointmentDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->foreign(AppointmentDefinition::EXPERT_ID)->references(ExpertDefinition::ID)
                ->on(ExpertDefinition::TABLE_NAME);

            $table->foreign(AppointmentDefinition::USER_ID)->references(UserDefinition::ID)
                ->on(UserDefinition::TABLE_NAME);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(AppointmentDefinition::TABLE_NAME);
    }
}

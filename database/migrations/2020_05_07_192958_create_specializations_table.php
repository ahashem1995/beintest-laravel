<?php

use App\Entities\SpecializationDefinition;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecializationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SpecializationDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->increments(SpecializationDefinition::ID);
            $table->string(SpecializationDefinition::NAME)->unique();
            $table->string(SpecializationDefinition::DESCRIPTION)->nullable();
            $table->unsignedInteger(SpecializationDefinition::PARENT_ID)->nullable();

            $table->timestamps();
        });

        Schema::table(SpecializationDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->foreign(SpecializationDefinition::PARENT_ID)->references(SpecializationDefinition::ID)
                ->on(SpecializationDefinition::TABLE_NAME);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(SpecializationDefinition::TABLE_NAME);
    }
}

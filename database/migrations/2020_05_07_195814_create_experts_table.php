<?php

use App\Entities\CountryDefinition;
use App\Entities\ExpertDefinition;
use App\Entities\SpecializationDefinition;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ExpertDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->increments(ExpertDefinition::ID);
            $table->string(ExpertDefinition::FIRST_NAME);
            $table->string(ExpertDefinition::LAST_NAME);
            $table->string(ExpertDefinition::EMAIL)->nullable()->unique();
            $table->string(ExpertDefinition::MOBILE_NUMBER)->nullable()->unique();
            $table->string(ExpertDefinition::PROFILE_PICTURE)->nullable();
            $table->string(ExpertDefinition::TIMEZONE)->default('');
            $table->unsignedInteger(ExpertDefinition::COUNTRY_ID);
            $table->unsignedInteger(ExpertDefinition::SPECIALIZATION_ID);

            $table->timestamps();
        });

        Schema::table(ExpertDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->foreign(ExpertDefinition::SPECIALIZATION_ID)->references(SpecializationDefinition::ID)
                ->on(SpecializationDefinition::TABLE_NAME);

            $table->foreign(ExpertDefinition::COUNTRY_ID)->references(CountryDefinition::ID)
                ->on(CountryDefinition::TABLE_NAME);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(SpecializationDefinition::TABLE_NAME);
    }
}

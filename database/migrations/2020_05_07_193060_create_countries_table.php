<?php

use App\Entities\CountryDefinition;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CountryDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->increments(CountryDefinition::ID);
            $table->string(CountryDefinition::NAME);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CountryDefinition::TABLE_NAME);
    }
}
